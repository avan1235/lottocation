package com.nerdy.lottocation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nerdy.lottocation.StaticData.API_URL;
import static com.nerdy.lottocation.StaticData.MAX_RAND;
import static com.nerdy.lottocation.StaticData.MIN_RAND;

public class JoinActivity extends AppCompatActivity {

    NumberPicker numberpicker;
    TextView textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        final RequestQueue queue = Volley.newRequestQueue(this);

        numberpicker = findViewById(R.id.numberPicker);
        numberpicker.setMinValue(MIN_RAND);
        numberpicker.setMaxValue(MAX_RAND);

        final Button startGame = findViewById(R.id.buttonStart);

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = ((EditText)findViewById(R.id.nameForm)).getText().toString();
                final String surname = ((EditText)findViewById(R.id.surnameForm)).getText().toString();
                final String num = Integer.toString(numberpicker.getValue());
                StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                        API_URL+"register.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject JSONresponse=new JSONObject(response);
                                    String status=JSONresponse.getString("status");
                                    if(status.equals("ok"))
                                        startTheGame(JSONresponse.getString("data"));
                                    else
                                        Toast.makeText(getApplicationContext(), "Nie możesz dołączyć do gry", Toast.LENGTH_SHORT).show();
                                }catch(Exception e){}
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Błąd połącznia z serwerem", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/x-www-form-urlencoded; charset=UTF-8";
                    }

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> postParam = new HashMap<>();
                        postParam.put("name", name);
                        postParam.put("surname", surname);
                        postParam.put("number", num);
                        return postParam;
                    }
                };
                queue.add(jsonObjRequest);
            }
        });
    }

    public void startTheGame(String sessionId) {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("SESSID", sessionId);
        startActivity(intent);

    }
}


