package com.nerdy.lottocation;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polygon;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static com.nerdy.lottocation.StaticData.API_URL;


public class MapActivity extends AppCompatActivity {

    private MapView map;

    final int MILLIS_UPDATE_MAP = 5000;
    final int METERS_UPDATE_MAP = 10;
    final int PERIOD_REFRESH = 5000;

    final int PERIOD_REFRESH_PROGRESS = 1000;

    final int SECONDS_TO_CHANGE_MAP = 60;

    boolean gotFfinalMessage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        final RequestQueue queue = Volley.newRequestQueue(this);

        requestLocationPermission();
        {
            // GPS enabled

            Context ctx = getApplicationContext();
            Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

            setContentView(R.layout.activity_map);

            map = findViewById(R.id.map);
            map.setTileSource(TileSourceFactory.MAPNIK);

            map.setBuiltInZoomControls(true);
            map.setMultiTouchControls(true);

            IMapController mapController = map.getController();
            mapController.setZoom(7.2);
            GeoPoint startPoint = new GeoPoint(52.1143, 19.3036);
            mapController.setCenter(startPoint);

            final GpsMyLocationProvider imlp = new GpsMyLocationProvider(this.getBaseContext());
            imlp.setLocationUpdateMinDistance(METERS_UPDATE_MAP);
            imlp.setLocationUpdateMinTime(MILLIS_UPDATE_MAP);

            final MyLocationNewOverlay myLocationNewOverlay = new
                    MyLocationNewOverlay(imlp, map);

            final ProgressBar progressBar = findViewById(R.id.progressBar3);
            progressBar.getProgressDrawable().setColorFilter(
                    Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN);

            myLocationNewOverlay.setDrawAccuracyEnabled(true);

            map.getOverlays().add(myLocationNewOverlay);

            new Timer().scheduleAtFixedRate(new TimerTask(){
                @Override
                public void run(){
                    // DELETING OLD POLYGONS ON MAP

                    for(int i=map.getOverlays().size()-1;i>0;i--)
                        map.getOverlayManager().remove(i);

                    // SEND LOCALIZATION DATA TO SERVER

                    StringRequest jsonObjRequestSend = new StringRequest(Request.Method.POST,
                            API_URL+"changeLatLon.php?PHPSESSID="+getIntent().getStringExtra("SESSID"),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                        @Override
                        public String getBodyContentType() {
                            return "application/x-www-form-urlencoded; charset=UTF-8";
                        }

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> postParam = new HashMap<>();
                                if(imlp.getLastKnownLocation()!=null && imlp.getLastKnownLocation().getAltitude()>0 && imlp.getLastKnownLocation().getLongitude()>0) {
                                    postParam.put("lat", Double.toString(imlp.getLastKnownLocation().getLatitude()));
                                    postParam.put("lon", Double.toString(imlp.getLastKnownLocation().getLongitude()));
                                }
                            return postParam;
                        }
                    };
                    queue.add(jsonObjRequestSend);

                    // GET THE DATA FROM SERVER

                    StringRequest jsonObjRequestGet = new StringRequest(Request.Method.GET,
                            API_URL+"mapStatus.php?PHPSESSID="+getIntent().getStringExtra("SESSID"),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject JSONresponse=new JSONObject(response);
                                        JSONArray data = JSONresponse.getJSONArray("data");
                                        for (int i = 0; i < data.length(); i++){
                                            double lat = data.getJSONObject(i).getDouble("lat");
                                            double lon = data.getJSONObject(i).getDouble("lon");
                                            double radius = data.getJSONObject(i).getDouble("radius");
                                            drawCircle(lat, lon, radius);
                                        }
                                        map.getOverlays().add(myLocationNewOverlay);
                                        int gotStatus = JSONresponse.getInt("game");
                                        if (gotStatus != 0 && !gotFfinalMessage){
                                            gotFfinalMessage = true;
                                            AlertDialog.Builder builder;
                                            builder = new AlertDialog.Builder(MapActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                                            builder.setTitle("Rozgrywka zakończona")
                                                    .setMessage(gotStatus < 0 ? "Niestety tym razem się nie udało.\nSpróbuj ponownie jutro" : "Brawo!\nWygrałeś dziś główną nagrodę!")
                                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            finish();
                                                        }
                                                    })
                                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            finish();
                                                        }
                                                    })
                                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                                    .show();
                                        }
                                    }
                                    catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {};
                    queue.add(jsonObjRequestGet);
                }
            },0,PERIOD_REFRESH);

            new Timer().scheduleAtFixedRate(new TimerTask(){
                @Override
                public void run(){
                    // GET THE DATA FROM SERVER
                    StringRequest jsonObjRequestGetTime = new StringRequest(Request.Method.GET,
                            API_URL+"timeToAction.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject data = new JSONObject(response);
                                        int time = data.getJSONObject("data").getInt("timeToAction");
                                        progressBar.setProgress((int)((1-((double)time/(double)SECONDS_TO_CHANGE_MAP))*100));
                                    }
                                    catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {};
                    queue.add(jsonObjRequestGetTime);
                }
            },0,PERIOD_REFRESH_PROGRESS);
        }
    }

    final static int REQUEST_LOCATION_PERMISSION = 99;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    public void requestLocationPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if(EasyPermissions.hasPermissions(this, perms))
            Toast.makeText(this, "Przyznano uprawnienia GPS", Toast.LENGTH_SHORT).show();
        else
            EasyPermissions.requestPermissions(this, "Przyznaj aplikacji dostep do GPS", REQUEST_LOCATION_PERMISSION, perms);
    }

    public void drawCircle(double lat, double lon, double radiusMeters){
        GeoPoint pt = new GeoPoint(lat, lon);

        //just in case the point is off the map, let's fix the coordinates
        if (pt.getLongitude() < -180)
            pt.setLongitude(pt.getLongitude() + 360);
        if (pt.getLongitude() > 180)
            pt.setLongitude(pt.getLongitude() - 360);
        //latitude is a bit harder. see https://en.wikipedia.org/wiki/Mercator_projection
        if (pt.getLatitude() > 85.05112877980659)
            pt.setLatitude(85.05112877980659);
        if (pt.getLatitude() < -85.05112877980659)
            pt.setLatitude(-85.05112877980659);

        List<GeoPoint> circle = Polygon.pointsAsCircle(pt, radiusMeters);
        Polygon p = new Polygon(map);
        p.setOnClickListener(new Polygon.OnClickListener() {
            @Override
            public boolean onClick(Polygon polygon, MapView mapView, GeoPoint eventPos) {
                return false;
            }
        });
        p.setPoints(circle);
        p.setStrokeColor(getColor(R.color.colorAccent));
        p.setStrokeWidth((float) (Math.log(radiusMeters)/3.));
        map.getOverlayManager().add(p);
        map.invalidate();
    }

    @Override
    public void onResume(){
        super.onResume();
        map.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
        map.onPause();
    }
}
